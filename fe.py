import json
import csv
from os import removedirs
import re
import traceback
import jieba.posseg as pseg
keywords = ["程序合法", "驳回", "具体行政行为", "第三人", "法定期限", "不予受理", "违法行为", "利害关系", "本案争议", "变更", "工伤", "公积金", "建设用地", "房屋征收", "所有权", "鉴定", "拆迁", "自然资源", "土地征收", "合同", "授权", "管辖", "履行法定职责", "土地使用权", "宅基地", "不可抗力", "建设工程", "行政拘留", "确权", "没收", "房屋拆迁", "不动产", "交通事故", "财产权"]

# def dictcount(sdata, keystr):
#     if sdata.has_key(keystr):
#         return sdata[keystr]
def featureExtraction():
    fd = open("total.json", "r")
    csvfd = open("wenshu.csv", "w", newline = '')
    fieldnames = ["法院地区", "法院层级", "年份", "结果", "第几审", "法条条目", "法条根据"]
    fieldnames += keywords
    writer = csv.DictWriter(csvfd, fieldnames=fieldnames)
    writer.writeheader()
    alllines = fd.readlines()
    succn = 0
    failn = 0
    for line in alllines:
        try:
            jsdata = json.loads(line)
            rsdict = {}

            lawentries = jsdata['s47']
            for le in lawentries:
                rsdict['法条条目'] = le['tkx']
                rsdict['法条根据'] = le['fgmc']
                break

            if (jsdata['s2'].count("最高人民法院") > 0):
                rsdict["法院地区"] = 0
                rsdict["法院层级"] = 0
            elif jsdata['s2'].count("高级人民法院"):
                fyg = re.match('(.+?)(高级人民法院)', jsdata['s2']) 
                rsdict["法院地区"] = fyg.group(1)
                rsdict["法院层级"]  = 1
            elif jsdata['s2'].count("中级人民法院"):
                fyg = re.match('(.+?)(中级人民法院)', jsdata['s2']) 
                rsdict["法院地区"] = fyg.group(1)
                rsdict["法院层级"]  = 2
            else :
                fyg = re.match('(.+?)(人民法院)', jsdata['s2']) 
                rsdict["法院地区"] = fyg.group(1)
                rsdict["法院层级"]  = 3


            #year
            rsdict["年份"] = re.match('[0-9][0-9][0-9][0-9]', jsdata['s31']).group()

            #结果
            searchBill = re.search("案件受理费.*?。</div>", jsdata["qwContent"])
            if searchBill == None:
                searchBill = re.search("[本案|案件].*?费用?人?民?币?[0-9]+元.*?由.*?负?承?担", jsdata["qwContent"])  
                if searchBill == None:
                    searchBill = re.search("本案不收取.*?费。</div>", jsdata["qwContent"])  


            rsdict["结果"] = 0 #政府输
            if searchBill == None:
                searchBill = re.search("维持被告.*?上诉", jsdata["qwContent"])  
                if searchBill != None:
                    rsdict["结果"] = 1
            else:
                affordbill = searchBill.group()
                pair_word_list = pseg.lcut(affordbill)
                for eve_word, wordtype in pair_word_list:
                    if wordtype == 'nr':
                        rsdict["结果"] = 1
                
            # if(affordbill.find("人民政府负担") >= 0):
            #     rsdict["结果"] = 0 #政府输
            # else:
            #     rsdict["结果"] = 1
            
            #第几审
            if(jsdata['s9'].count("一审") > 0):
                rsdict["第几审"] = 1
            else :
                if (jsdata['s9'].count("二审") > 0):
                    rsdict["第几审"] = 2
                else :
                    rsdict["第几审"] = 3

            #keywords
            for kw in keywords:
                # rsdict[kw] = jsdata['s23'].count(kw) + jsdata['s25'].count(kw) + jsdata['s26'].count(kw) + jsdata['s27'].count(kw) + jsdata['s28'].count(kw) + jsdata['s25'].count(kw) + jsdata['s17'].count(kw) + jsdata['qwContent'].count(kw)
                rsdict[kw] = line.count(kw)
                # print("%s : %d\n" % (kw, rsdict[kw]))

        # break
        except Exception as e:
            traceback.print_exc()
            failn += 1
            # print(line)
            # break
        else:
            succn += 1
            writer.writerow(rsdict)
        
    print("failn:%d succn:%d" % (failn, succn))
    return

if __name__ == '__main__':
    featureExtraction()
