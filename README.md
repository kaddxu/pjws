### 列说明
法院地区：0-最高人民法院

法院层级：0—最高人民法院，1-高级，2-中级，3-基层

结果：0-政府输，1-政府赢

第几审：1-一审，2-二审，3-终审

通用关键字：出现频数



### PUSH
1. vi ~/.ssh/id_rsa.pub

2. upload ssh key

3. git remote set-url origin git@gitlab.com:kaddxu/pjws.git